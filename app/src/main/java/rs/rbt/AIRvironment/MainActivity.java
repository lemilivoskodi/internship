package rs.rbt.AIRvironment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import rs.rbt.AIRvironment.api.ApiCallbackListener;
import rs.rbt.AIRvironment.api.ApiManager;
import rs.rbt.AIRvironment.api.model.EnvironmentDataModel;
import rs.rbt.AIRvironment.utils.FormatterUtils;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private static final int TIME_IN_SECONDS_WHEN_DATA_WILL_BE_REFRESHED = 5;

    private View mProgressOverlayView;
    private TextView mButtonShowHistory;
    private TextView mTextViewDateTime;
    private TextView mTextViewTemperature;
    private TextView mTextViewHumidity;
    private TextView mTextViewPollution;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();

        mButtonShowHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "It will be implemented in the future", Toast.LENGTH_SHORT).show();
            }
        });

        showProgressOverlay();
        scheduleEnvironmentDataUpdate();
    }

    private void executeApiCall() {
        ApiManager.getValues(new ApiCallbackListener<EnvironmentDataModel>() {
            @Override
            public void onSuccess(EnvironmentDataModel response) {
                Log.d(TAG, "response: " + response);
                showValues(response);
                hideProgressOverlay();
            }

            @Override
            public void onError(String error) {
                hideProgressOverlay();
                showError(error);
            }
        });
    }

    private void scheduleEnvironmentDataUpdate() {
        ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate
                (new Runnable() {
                    public void run() {
                        executeApiCall();
                    }
                }, 0, TIME_IN_SECONDS_WHEN_DATA_WILL_BE_REFRESHED, TimeUnit.SECONDS);
    }

    private void showError(String errorMessage) {
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.Ooops))
                .setMessage(errorMessage)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(getString(R.string.Ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .show();
    }

    private void initViews() {
        mProgressOverlayView = findViewById(R.id.progress_overlay);
        mButtonShowHistory = findViewById(R.id.button_showHistory);
        mTextViewDateTime = findViewById(R.id.textView_Datetime);
        mTextViewTemperature = findViewById(R.id.textView_Temperature);
        mTextViewHumidity = findViewById(R.id.textView_Humidity);
        mTextViewPollution = findViewById(R.id.textView_Pollution);
    }

    private void showValues(EnvironmentDataModel values) {
        String formattedDate = getString(R.string.Last_update) + " " + FormatterUtils.formatTimestamp(values.getTimestamp());
        String formattedTemperature = FormatterUtils.formatTemperature(values.getTemperature());
        String formattedHumidity = FormatterUtils.formatHumidity(values.getHumidity());

        mTextViewDateTime.setText(formattedDate);
        mTextViewTemperature.setText(formattedTemperature);
        mTextViewHumidity.setText(formattedHumidity);
        mTextViewPollution.setText(String.valueOf(values.getPollution()));
    }

    private void showProgressOverlay() {
        mProgressOverlayView.setVisibility(View.VISIBLE);
    }

    private void hideProgressOverlay() {
        mProgressOverlayView.setVisibility(View.GONE);
    }

}
