package rs.rbt.AIRvironment.api;

public interface ApiCallbackListener<T> {

    void onSuccess(T response);

    void onError(String error);

}
