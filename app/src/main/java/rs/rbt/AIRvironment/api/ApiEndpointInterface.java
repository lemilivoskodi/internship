package rs.rbt.AIRvironment.api;

import retrofit2.Call;
import retrofit2.http.GET;
import rs.rbt.AIRvironment.api.model.EnvironmentDataModel;

public interface ApiEndpointInterface {

    @GET("/measurement/latest")
    Call<EnvironmentDataModel> getValues();

}
