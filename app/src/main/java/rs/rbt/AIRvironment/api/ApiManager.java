package rs.rbt.AIRvironment.api;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rs.rbt.AIRvironment.api.model.EnvironmentDataModel;

public class ApiManager {

    private static final String TAG = "ApiManager";
    private static final String BASE_URL = "https://boiling-scrubland-99747.herokuapp.com/";

    private static Retrofit retrofitBuilder = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build();

    public static void getValues(final ApiCallbackListener listener) {
        // create endpoint interface
        ApiEndpointInterface endpointInterface = retrofitBuilder.create(ApiEndpointInterface.class);

        // create callback success/failure methods
        Callback<EnvironmentDataModel> callback = new Callback<EnvironmentDataModel>() {
            @Override
            public void onResponse(Call<EnvironmentDataModel> call, Response<EnvironmentDataModel> response) {
                Log.d(TAG, "Response: " + response);
                if (response.isSuccessful()) {
                    if (listener != null) {
                        listener.onSuccess(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<EnvironmentDataModel> call, Throwable t) {
                Log.d(TAG, "Failure: " + t.toString());
                listener.onError(t.getMessage());
            }
        };

        // prepare API call
        Call<EnvironmentDataModel> call = endpointInterface.getValues();

        // execute API call
        call.enqueue(callback);
    }

}
