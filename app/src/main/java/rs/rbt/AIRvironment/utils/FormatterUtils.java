package rs.rbt.AIRvironment.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class FormatterUtils {

    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZZZZZ";
    private static final char DEGREE_UNICODE_SYMBOL = '\u00B0';
    private static final char PERCENT_UNICODE_SYMBOL = '\u0025';

    public static String formatTimestamp(final String timestamp) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT, Locale.US);
            Date date = simpleDateFormat.parse(timestamp);
            return date.toString();
        } catch (ParseException e) {
            return timestamp;
        }
    }

    public static String formatTemperature(final double temperature) {
        return String.valueOf((int) temperature) + DEGREE_UNICODE_SYMBOL;
    }

    public static String formatHumidity(final double humidity) {
        return String.valueOf((int) humidity) + PERCENT_UNICODE_SYMBOL;
    }

}
